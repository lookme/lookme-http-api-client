require 'logger'
require 'json'

require 'net/http'
require 'uri'

require 'lookme_logging'

require 'lookme_http_api_client/base_http_error'
require 'lookme_http_api_client/timeout_error'
require 'lookme_http_api_client/api_error'
require 'lookme_http_api_client/http_client_config'
require 'lookme_http_api_client/request'
require 'lookme_http_api_client/response'
require 'lookme_http_api_client/mock_response'
require 'lookme_http_api_client/http_client'
require 'lookme_http_api_client/http_client_support'
require 'lookme_http_api_client/net_http_client'
require 'lookme_http_api_client/net_http_response'
require 'lookme_http_api_client/execution_logging'
require 'lookme_http_api_client/oauth2/o_auth2_error'
require 'lookme_http_api_client/oauth2/access_token'
require 'lookme_http_api_client/oauth2/oauth2_client_config'
require 'lookme_http_api_client/oauth2/o_auth2_client'
require 'lookme_http_api_client/rails/request_context_provider'

module LookmeHttpApiClient
  VERSION = '0.0.1'

  METHOD_GET = :get
  METHOD_POST = :post
  METHOD_PUT = :put
  METHOD_PATCH = :patch
  METHOD_DELETE = :delete

  HEADER_KEY_CONTENT_TYPE = 'Content-Type'
  HEADER_KEY_USER_AGENT = 'User-Agent'
  HEADER_KEY_AUTHORIZATION = 'Authorization'

  CONTENT_TYPE_JSON = 'application/json'
  CONTENT_TYPE_WWW_FORM = 'application/x-www-form-urlencoded'
  CONTENT_TYPE_FORM_DATA = 'multipart/form-data'

  STATUS_CODE_OK = 200
end
module LookmeHttpApiClient
  class HttpClient
    PROVIDER_NET_HTTP = :net_http

    # @param [LookmeHttpApiClient::Request] _request
    def execute(_request)
      raise ::NotImplementedError
    end

    @providers = {}

    class << self
      def build(client_config, options = {})
        provider = options[:provider] || PROVIDER_NET_HTTP
        http_client = build_http_client(provider, client_config)
        http_client = with_oauth2(http_client, client_config, options[:oauth2_config])
        with_execution_logging(http_client, client_config)
      end

      private

      def with_execution_logging(http_client, client_config)
        ExecutionLogging.new(http_client, client_config)
      end

      def with_oauth2(http_client, client_config, oauth2_config)
        return http_client if oauth2_config.nil?
        OAuth2::OAuth2Client.new(http_client, client_config, oauth2_config)
      end

      def build_http_client(provider, client_config)
        case provider
          when PROVIDER_NET_HTTP
            NetHttpClient.new(client_config)
          else
            raise ::ArgumentError, "unexpected provider #{provider}"
        end
      end
    end
  end
end
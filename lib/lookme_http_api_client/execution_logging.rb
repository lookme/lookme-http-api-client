module LookmeHttpApiClient
  class ExecutionLogging
    LOG_TAG = self.name

    # @param [LookmeHttpApiClient::HttpClient] http_client
    # @param [LookmeHttpApiClient::HttpClientConfig] config
    def initialize(http_client, config)
      @http_client = http_client
      @config = config
    end

    # @param [LookmeHttpApiClient::Request] request
    def execute(request)
      logger.info(LOG_TAG) {'%s %s://%s:%s%s' % [
          request.method.to_s.upcase,
          @config.schema,
          @config.host,
          @config.port,
          request.build_absolute_path(@config.base_path)
      ]}
      logger.debug(LOG_TAG) {"headers #{request.headers}"}
      logger.debug(LOG_TAG) {"query parameters #{request.query_params}"} if request.has_query_param?
      logger.debug(LOG_TAG) {"form parameters #{request.form_params}"} if request.has_form_param?
      logger.debug(LOG_TAG) {"body #{request.body}"} if request.body_set?
      start_time = Time.now
      begin
        response = @http_client.execute(request)
        logger.info(LOG_TAG) {'response code %d, duration %dms' % [
            response.status_code,
            evaluate_duration_ms(start_time)
        ]}
        response
      rescue => e
        logger.info(LOG_TAG) {'duration %dms' % [evaluate_duration_ms(start_time)]}
        raise e
      end
    end

    private

    def logger
      @config.logger
    end

    def evaluate_duration_ms(start_time)
      ((Time.now - start_time) * 1000).to_i
    end
  end
end
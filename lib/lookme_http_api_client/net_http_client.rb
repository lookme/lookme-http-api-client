module LookmeHttpApiClient
  class NetHttpClient < HttpClient
    attr_reader :client_config

    # @param [LookmeHttpApiClient::HttpClientConfig] client_config
    def initialize(client_config)
      @client_config = client_config
    end

    # @param [LookmeHttpApiClient::Request] request
    def execute(request)
      net_http_request = build_net_http_request(request)
      net_http_response = nil
      begin
        # TODO, use connection pool
        net_http_response = ::Net::HTTP.start(
            @client_config.host, @client_config.port,
            use_ssl: (@client_config.schema == 'https')) do |http|
          http.open_timeout = @client_config.timeout
          http.read_timeout = @client_config.timeout
          http.request(net_http_request)
        end
      rescue Net::ReadTimeout
        raise TimeoutError, 'read timeout'
      end
      NetHttpResponse.new(net_http_response)
    end

    private

    # @param [LookmeHttpApiClient::Request] request
    # @return [Net::HTTPRequest]
    def build_net_http_request(request)
      klass = determine_net_http_request_class(request.method)
      r = klass.new(build_path_with_query(request), merge_headers(request))
      if r.request_body_permitted?
        if request.content_type == CONTENT_TYPE_WWW_FORM || request.content_type == CONTENT_TYPE_FORM_DATA
          r.set_form(request.form_params, request.content_type)
        else
          r.body = request.body
        end
      end
      r
    end

    def merge_headers(request)
      headers = {}
      @client_config.default_headers.each {|k, v| headers[k.to_s] = v }
      request.headers.each {|k, v| headers[k.to_s] = v }
      headers
    end

    # @param [LookmeHttpApiClient::Request] request
    # @return [String]
    def build_path_with_query(request)
      absolute_path = request.build_absolute_path(@client_config.base_path)
      if request.has_query_param?
        query_params = request.query_params.reject {|k, v| v.nil?}
        "#{absolute_path}?#{::URI.encode_www_form(query_params)}"
      else
        "#{absolute_path}"
      end
    end

    # @param [Symbol] method
    # @return [Class]
    def determine_net_http_request_class(method)
      case method
        when METHOD_GET
          ::Net::HTTP::Get
        when METHOD_POST
          ::Net::HTTP::Post
        when METHOD_PUT
          ::Net::HTTP::Put
        when METHOD_PATCH
          ::Net::HTTP::Patch
        when METHOD_DELETE
          ::Net::HTTP::Delete
        else
          raise ::ArgumentError, "unsupported http method #{method}"
      end
    end
  end
end
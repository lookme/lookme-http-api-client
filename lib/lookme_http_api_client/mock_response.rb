module LookmeHttpApiClient
  class MockResponse < Response
    attr_writer :status_code # Fixnum
    attr_writer :headers # Hash
    attr_writer :body # String

    def initialize
      @status_code = STATUS_CODE_OK
      @headers = {}
      @body = ''
    end

    def status_code
      @status_code
    end

    def headers
      @headers
    end

    def body_as_string
      @body
    end
  end
end
module LookmeHttpApiClient
  class HttpClientConfig
    attr_reader :schema # String
    attr_accessor :host # String e.g www.example.com
    attr_accessor :port # Fixnum e.g 80, 443
    attr_reader :base_path # String

    attr_accessor :timeout # Fixnum
    attr_accessor :logger # Logger

    attr_accessor :default_headers # Hash

    def initialize
      @schema = 'http'
      @host = nil
      @port = 80
      @base_path = nil

      @timeout = 0
      @logger = ::Logger.new(STDOUT)

      @default_headers = {HEADER_KEY_USER_AGENT => "LookmeHttpApiClient #{VERSION}"}
    end

    # @param [String] schema e.g http, https
    def schema=(schema)
      @schema = schema.downcase
    end

    # @param [String] base_path e.g /foo
    def base_path=(base_path)
      if base_path == '/'
        @base_path = ''
      elsif base_path.start_with?('/')
        @base_path = base_path[1..-1]
      else
        @base_path = base_path
      end
    end
  end
end
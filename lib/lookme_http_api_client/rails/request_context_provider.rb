module LookmeHttpApiClient
  module Rails
    module RequestContextProvider
      def header_request_id
        key = LookmeLogging::Rails::RequestContextCollector::KEY_REQUEST_ID
        request_id = LookmeLogging::MDC.get(key)
        if request_id
          {'X-Request-Id' => LookmeLogging::MDC.get(key)}
        else
          {}
        end
      end
    end
  end
end
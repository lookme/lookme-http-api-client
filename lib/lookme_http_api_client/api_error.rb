module LookmeHttpApiClient
  class ApiError < BaseHttpError
    def initialize(message, response)
      super(message)
      @response = response
    end

    def message
      if @response.status_code >= 400 && @response.status_code < 500
        "#{super}, status code #{@response.status_code}, response #{@response.body_as_string}"
      else
        "#{super}, status code #{@response.status_code}"
      end
    end

    def status_code
      @response.status_code
    end

    def response_content
      @response.body_as_string
    end
  end
end
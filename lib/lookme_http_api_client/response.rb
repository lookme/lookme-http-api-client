module LookmeHttpApiClient
  class Response
    # @return [Fixnum]
    def status_code
      raise ::NotImplementedError
    end

    # @return [Boolean]
    def success?
      status_code == STATUS_CODE_OK
    end

    # @return [Hash<String,String>]
    def headers
      raise ::NotImplementedError
    end

    # @return [String]
    def content_type
      headers[HEADER_KEY_CONTENT_TYPE]
    end

    # @return [String]
    def body_as_string
      raise ::NotImplementedError
    end
  end
end
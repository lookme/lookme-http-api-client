module LookmeHttpApiClient
  module HttpClientSupport
    attr_writer :http_client

    def execute_http_request(hash)
      request = Request.new(hash)
      @http_client.execute(request)
    end

    def http_get(path, query_params = {})
      request = Request.new
      request.method = METHOD_GET
      request.path = path
      request.headers = header_request_id
      request.query_params = query_params
      @http_client.execute(request)
    end

    def http_post(path, form_params)
      request = Request.new
      request.method = METHOD_POST
      request.path = path
      request.form_params = form_params
      request.headers = header_request_id.merge(HEADER_KEY_CONTENT_TYPE => CONTENT_TYPE_WWW_FORM)
      @http_client.execute(request)
    end

    def http_post_json(path, json)
      request = Request.new
      request.method = METHOD_POST
      request.path = path
      request.body = json
      request.headers = header_request_id.merge(HEADER_KEY_CONTENT_TYPE => CONTENT_TYPE_JSON)
      @http_client.execute(request)
    end

    def http_put_json(path, json)
      request = Request.new
      request.method = METHOD_PUT
      request.path = path
      request.body = json
      request.headers = header_request_id.merge(HEADER_KEY_CONTENT_TYPE => CONTENT_TYPE_JSON)
      @http_client.execute(request)
    end

    def http_patch_json(path, json)
      request = Request.new
      request.method = METHOD_PATCH
      request.path = path
      request.body = json
      request.headers = header_request_id.merge(HEADER_KEY_CONTENT_TYPE => CONTENT_TYPE_JSON)
      @http_client.execute(request)
    end

    def http_delete(path)
      request = Request.new
      request.method = METHOD_DELETE
      request.path = path
      request.headers = header_request_id
      @http_client.execute(request)
    end

    def header_request_id
      {}
    end
  end
end

module LookmeHttpApiClient
  class Request
    attr_accessor :method # Symbol
    attr_accessor :absolute_path # String e.g /, /foo/bar
    attr_accessor :path # String e.g /bar
    attr_accessor :headers # Hash<String, String>
    attr_accessor :query_params # Hash<String, String>
    attr_accessor :form_params # Hash<String, String>
    attr_accessor :body # String

    def initialize(hash = {})
      @method = hash[:method]
      @absolute_path = hash[:absolute_path]
      @path = hash[:path]
      @headers = hash[:headers] || {}
      @query_params = hash[:query_params] || {}
      @form_params = hash[:form_params] || {}
      @body = hash[:body]
    end

    # @return [String]
    def content_type
      @headers[HEADER_KEY_CONTENT_TYPE]
    end

    # @return [Boolean]
    def has_query_param?
      !@query_params.empty?
    end

    # @return [Boolean]
    def has_form_param?
      !@form_params.empty?
    end

    # @return [Boolean]
    def body_set?
      @body != nil
    end

    # @param [String] base_path e.g '', 'foo', 'foo/bar'
    # @return [String]
    def build_absolute_path(base_path)
      if @absolute_path
        @absolute_path
      else
        if base_path && !base_path.empty?
          "/#{base_path}#{@path}"
        else
          @path
        end
      end
    end
  end
end
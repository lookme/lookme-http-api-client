module LookmeHttpApiClient
  module OAuth2
    class AccessToken
      attr_reader :token # String
      attr_reader :type # String

      # @param [String] token
      def initialize(token, type)
        @token = token
        @type = type
      end

      # @return [String]
      def to_authorization
        "#{@type} #{@token}"
      end
    end
  end
end
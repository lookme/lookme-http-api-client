module LookmeHttpApiClient
  module OAuth2
    class OAuth2Client < HttpClient
      LOG_TAG = self.name

      # @param [LookmeHttpApiClient::HttpClient] underlying_client
      # @param [LookmeHttpApiClient::HttpClientConfig] http_client_config
      # @param [LookmeHttpApiClient::OAuth2::OAuth2ClientConfig] oauth2_config
      def initialize(underlying_client, http_client_config, oauth2_config)
        @underlying_client = underlying_client
        @http_client_config = http_client_config
        @oauth2_config = oauth2_config
        @access_token = oauth2_config.access_token
      end

      def execute(request)
        ensure_access_token!
        request.headers[HEADER_KEY_AUTHORIZATION] = @access_token.to_authorization
        @underlying_client.execute(request)
        # token refresh not supported now
      end

      private

      def ensure_access_token!
        return if @access_token

        if @oauth2_config.path_token_retrieve.nil?
          raise OAuth2Error, 'path token retrieve required'
        end

        request = LookmeHttpApiClient::Request.new(
            method: METHOD_POST,
            absolute_path: @oauth2_config.path_token_retrieve,
            headers: {
                HEADER_KEY_CONTENT_TYPE => CONTENT_TYPE_JSON
            },
            timeout: @http_client_config.timeout,
            body: {
                grant_type: @oauth2_config.grant_type,
                client_id: @oauth2_config.client_id,
                client_secret: @oauth2_config.client_secret,
                scope: @oauth2_config.scope,
            }.to_json
        )

        logger.info(LOG_TAG) {'POST %s://%s:%s%s' % [
            @http_client_config.schema,
            @http_client_config.host,
            @http_client_config.port,
            @oauth2_config.path_token_retrieve
        ]}
        logger.debug(LOG_TAG) {"headers #{request.headers}"}
        logger.debug(LOG_TAG) {"body #{request.body}"}
        response = @underlying_client.execute(request)
        if response.status_code != STATUS_CODE_OK
          raise OAuth2Error, "failed to retrieve token, status code #{response.status_code}"
        end

        response_content = response.body_as_string
        logger.debug(LOG_TAG) {"response #{response_content}"}
        json =::JSON.parse(response_content)
        @access_token = AccessToken.new(json['access_token'], json['token_type'])
      end

      def logger
        @http_client_config.logger
      end
    end
  end
end

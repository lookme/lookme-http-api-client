module LookmeHttpApiClient
  module OAuth2
    class OAuth2ClientConfig
      attr_accessor :client_id # String
      attr_accessor :client_secret # String
      attr_accessor :grant_type # String
      attr_accessor :scope # String (space separated)
      attr_accessor :path_token_retrieve # String
      attr_accessor :path_token_refresh # String
      attr_accessor :access_token # AccessToken

      def initialize
        @client_id = nil
        @client_secret = nil
        @grant_type = nil
        @scope = nil
        @path_token_retrieve = nil
        @path_token_refresh = nil
        @access_token = nil
      end

      def bearer_access_token=(token)
        @access_token = token.nil? ? nil : AccessToken.new(token, 'bearer')
      end
    end
  end
end

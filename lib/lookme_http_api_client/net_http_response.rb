module LookmeHttpApiClient
  class NetHttpResponse < Response
    # @param [Net::HTTPResponse] net_http_response
    def initialize(net_http_response)
      @net_http_response = net_http_response
    end

    # @return [Fixnum]
    def status_code
      Integer(@net_http_response.code)
    end

    # @return [Hash<String,String>]
    def headers
      headers = {}
      @net_http_response.each_header do |k, vs|
        headers[k] = vs
      end
      headers
    end

    def content_type
      @net_http_response['Content-Type']
    end

    # @return [String]
    def body_as_string
      raw_body = @net_http_response.body
      charset = parse_charset(@net_http_response['Content-Type'])
      charset ? raw_body.force_encoding(charset) : raw_body
    end

    private

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type
    # @param [String] content_type
    # @return [String]
    def parse_charset(content_type)
      return nil if content_type.nil?

      parts = content_type.split(';')
      return nil if parts.length <= 1

      charset = nil
      parts[1..parts.length].each do |part|
        index = part.index('=')
        next if index.nil?
        key = part[0...index].downcase.strip
        next if key != 'charset'
        charset = part[(index + 1)..-1]
        break
      end
      charset
    end
  end
end

# lookme-http-api-client

## 紹介

HTTP経由でAPIをやりとり用のライブラリです。

## 機能

* net/httpベース
* HTTP GET/POST/PUT/PATCH/DELETE対応
* OAuth2対応（refreshはまだ実装してない）
* リクエストID対応
* ログあり

## 使い方

### Quick Start

    client_config = LookmeHttpApiClient::HttpClientConfig.new
    client_config.host = 'www.google.com'
    client_config.base_path = '/'
    client_config.timeout = 3

    client = LookmeHttpApiClient::HttpClient.build(client_config)

    request = LookmeHttpApiClient::Request.new
    request.method = :get

    response = client.execute(request)

    puts response.status_code
    puts response.body_as_string

### 設定

クライアントの設定では、こういう項目があります。

* schema: http|https、デフォルトはhttp
* host: e.g api.example.com（HTTP API Clientを使う時、エンドポイントは基本変わらないので）
* port: デフォルトは80
* base_path: ベースパス、共通のURL prefix、'/'でも可
* timeout: タイムアウト、デフォルトは0、つまりタイムアウトなし
* default_headers: Hash<String, String>、デフォルトはクライアントのバージョンを含めるUserAgentのみ

もしOAuth2を使うと、別の設定クラスがあります。

    client_config = LookmeHttpApiClient::HttpClientConfig.new
    client_config.host = 'foo.example.com'
    client_config.base_path = '/'

    oauth2_config = LookmeHttpApiClient::OAuth2::OAuth2ClientConfig.new
    oauth2_config.bearer_access_token = 'some access token'
    oauth2_config.client_id = 'some client id'
    oauth2_config.client_secret = 'some client secret'
    oauth2_config.grant_type = 'client_credentials'
    oauth2_config.path_token_retrieve = '/oauth/token'

    LookmeHttpApiClient::HttpClient.build(client_config, oauth2_config: oauth2_config)

サーバー側のAPI Clientで、よくclient_credentialsを使いますので、いまはclient_credentialsだけ対応してます。
最初からbearer_access_tokenが入ってると、リクエストのヘッダーにAuthorizationを入れて送信します。
ないなら、401エラーが返された時、下4つの設定項目（client_id, client_secret, grant_type, path_token_retrieve）でトークンを取ります。
トークンの取得がライブラリ内でやりますので、使い側はとくに感じることはないです。

### RequestとHttpClientSupport

パラメータやヘッダーなどを入れて、APIエンドポイントとやり取りするためのクラスはRequestです。
Requestにいろいろ項目がありますが、よく使うパターンにお応じて、HttpClientSupportを用意しました。

実際のクライアントコード

    class FooClient
      include LookmeHttpApiClient::HttpClientSupport

      def find_foo(id)
        response = http_get("/foo/#{id}")
        if response.success?
          parse_foo(response.body_as_string)
        elsif response.status_code == 404
          nil
        else
          raise LookmeHttpApiClient::ApiError.new('failed to find foo', response)
        end
      end

      private

      def parse_foo(respone_content)
        # do stuff
      end
    end

    foo_client = FooClient.new
    foo_client.http_client = create_http_client
    foo_client.find_foo(1)

このクライアントではRequestは出てないですが、モジュールHttpClientSupportのメソッドhttp_getで裏で作成され、リクエストします。

http_getを含めてこれらのメソッドがあります。

* execute_http_request(hash)
* http_get(path, query_params = {})
* http_post(path, form_params)
* http_post_json(path, json)
* http_put_json(path, json)

HttpClientSupportを導入するころで、クラスが自動的にhttp_client変数がセットできるようになります。
クライアントを初期化する時は、http_clientを設定すれば使えます。

### Response

lookme-http-api-clientのレスポンスはこういうAPIがあります。

* status_code 数値
* success? ステータスコードは200かどうか
* headers レスポンスにあるHTTPのヘッダー
* content_type
* body_as_string

### リクエストID

APIサービスにアクセスする時、もしリクエストIDがあれば、サービス側でリクエストIDをログなどに出力し、調査しやすくなります。
lookme-api-http-clientでは、HttpClientSupportにもう1つのメソッドがあります。

    header_request_id

lookme-loggingと一緒に使う時、もう1つのモジュールを導入すれば、自動的にセットしてくれます。

    include LookmeHttpApiClient::Rails::RequestContextProvider

lookme-loggingでどうやってRailsのリクエストIDを取得するのは、lookme-loggingのドキュメントに参照してください。

まとめてみると、おすすめなクライアントコードは

    class FooClient
      include LookmeHttpApiClient::HttpClientSupport
      include LookmeHttpApiClient::Rails::RequestContextProvider
    end

こんな感じになります。

### ログ

LookmeHttpApiClient::HttpClient#buildでHTTPクライアントを作成する時、自動的にログ機能をついています。デフォルトは標準出力に出しますが、クライアントの設定で変えます。

    client_config = LookmeHttpApiClient::HttpClientConfig.new
    client_config.host = 'www.google.com'
    client_config.base_path = '/'
    client_config.timeout = 3
    client_config.logger = Logger.new('client.log')

    client = LookmeHttpApiClient::HttpClient.build(client_config)
